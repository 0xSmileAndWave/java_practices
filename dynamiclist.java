/*
Description: how dynamic lists works in java
*/
import java.io.*;
import java.util.*;

public class dynamiclist {

    public static void main(String[] args) {
        //List<String> names = new ArrayList<String>(); //  Also possible
        ArrayList<String> names = new ArrayList<String>();
        names.add("alice");
        names.add("hatter");
        names.add("cat");
        names.add("octopus");
        //System.out.println(names.get(2));   //  print third name
        //System.out.println(names);  //  prints whole list

        /*
        for (String n : names) {    //  print one by one
            System.out.println(n);
        }
         */
        //  Convert to String type arrays
		//String[] arr = names.toArray( new String[names.size()] ); //	like this also can
        String[] arr = new String[names.size()];
        arr = names.toArray(arr);

        for (String n : arr) {    //  print one by one
            System.out.println(n);
        }

    }
}
