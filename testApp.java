/*
Description: Practice using ObjectOutputStream and ObjectInputStream to handle arrays
*/
import java.io.*;

public class testApp {

    public static void main(String[] args) {
        int[] intArray = new int[3];
        intArray[0] = 1;
        intArray[1] = 2;
        intArray[2] = 3;

        for (int i = 0; i < intArray.length; i++) {
            //System.out.println(intArray[i]);
        }
        //  
        //  Writing to binary file !! example
        //
        try {
            ObjectOutputStream myStream = new ObjectOutputStream(new FileOutputStream("C:\\Users\\Laurrence\\Desktop\\data.dat"));
            myStream.writeObject(intArray);
            myStream.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("IOException thrown");
        }

        //
        //  Reading from binary file !! example
        //
        try {
            ObjectInputStream inputz = new ObjectInputStream(new FileInputStream("C:\\Users\\Laurrence\\Desktop\\data.dat"));
            int[] intArrayRead = (int[]) inputz.readObject(); //    Convert Object to Integer Array
            for (int i = 0; i < intArrayRead.length; i++) {
                System.out.println(intArrayRead[i]);
            }

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
